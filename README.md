Cоздайте пустую папку и склонируйте проект


git clone https://gitlab.com/antonmazun/wa_14_12_21.git

cd wa_14_12_21/

git checkout master


python -m venv env (windows)


virtualenv -p python3 env (ubuntu)

python3 -m venv env (macOS)

(linux/macOS) source env/bin/activate

(windows) cd env/Scripts/ -> activate

cd wa_24_11_django

pip install -r req.txt

python manage.py migrate

python manage.py runserver

